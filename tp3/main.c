//
//  main.c
//  tp3
//
//  Created by gaetan scarna on 08/10/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void exercice1(void);
void exercice2(void);
void exercice3(void);
void exercice4(void);
void exercice5(void);

void copy(char *chaine, char *chaine2);
void remplaceChaine(char chaine[]);
void remplacePointeur(char *chaine);

int main(int argc, const char * argv[]) {
//    exercice1();
//    exercice2();
//    exercice3();
    exercice4();
//    exercice5();
    return 0;
}


void exercice1(void){
    char chaine[100] = {"je m'appelle gaetan"};
    char chaine2[100];
    copy(chaine, chaine2);
    printf("La chaine 2 est : %s \n", chaine2);
}

void copy(char *chaine, char *chaine2){
    int i=0;
    while (*(chaine+i)!='\0') {
        *(chaine2+i)= *(chaine+i);
        i++;
    }
}

void exercice2(void){
    int N=10;
    int *t;
    t= (int*) malloc(sizeof(int)*N/2);
    int compt=0;
    for(int i=0; i<N ; i++){
        if(i%2 ==0){
            *(t+compt)=i;
            printf("%d\n", *(t+compt));
            compt++;
        }
        
    }
}


void exercice3(void){
    int A[10]={1,2,3,4,5,6,7,8,9,10};
    int *P1 =A;
    int *P2 = (A+9);
    int AIDE=0;
    int n=9;
    
    for (int i=0; i<10; i++) {
        printf("%d ", A[i]);
        
    }
    
    printf("\n");
    
    for (int i=1; i<6; i++) {
        AIDE= *P1;
        *P1=*P2;
        *P2=AIDE;
        n--;
        P1=(A+i);
        P2=(A+n);
    }
    
    for (int i=0; i<10; i++) {
        printf("%d ", A[i]);
        
    }
    printf("\n");
    
}

void exercice4(void){
    
    int x;
    int compt=0;
    int n=5;
    int *p;
    p= (int*) malloc(sizeof(int)*n);
    printf("Remplissez le tableau svp\n");
    for(int i=0; i<n;i++){
        scanf("%d", (p+i));
    }
   
    printf("Entrez la valeur a supprimer\n");
    scanf("%d", &x);
    printf("\n");
    for (int i=0; i<n; i++) {
        while(x == *(p+i)){
            for (int j=i; j<n-1; j++) {
                *(p+j)= *(p+j+1);
            }
            compt++;
        }
    }
    n-=compt;
    p=realloc(p, sizeof(int)*n);
    for(int i=0; i<n;i++){
        printf("%d\n", *(p+i));
    }
    free(p);
}

void exercice5(void){
    char chaine[100] = {"je m'appelle gaetan"};
    remplaceChaine(chaine);
//    remplacePointeur(chaine);
    printf("%s\n", chaine);
}

void remplaceChaine(char chaine[]){
    for (int i=0; i<strlen(chaine); i++) {
        if(' '== chaine[i]){
            chaine[i]='_';
        }
    }
}

void remplacePointeur(char *chaine){
    for (int i=0; i<strlen(chaine); i++) {
        if(' '== *(chaine+i)){
            *(chaine+i)='_';
        }
    }
}
